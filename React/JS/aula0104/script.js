import quadrado from './quadrado.js';
import numeroAleatorio from './numeroAleatorio.js';

console.log(quadrado.areaQuadrado(5));
console.log(numeroAleatorio());

//sincronização asincrona atraves do fetch

fetch('https://ranekapi.origamid.dev/wp-json/api/produto')
  .then((response) => response.json())
  .then((json) => {
    console.log(json);
  });
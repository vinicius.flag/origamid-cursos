//objeto
const menu = {
  seletor: '.principal'//propriedade 
}

console.log(menu.seletor); //para acessar a propriedade do objeto com um ponto

//função cumum 
function upperName(name) {
  return name.toUpperCase();
}

console.log(upperName('vinicius'));

//arrowFuncion
// const lowerName = () => {
 
// }

//outra forma de fazer uma função é atraves de uma expressão
// const toUpperName = function (name) {

// }
const toUpperName = (name) => {
 return name.toUpperCase();
}
// const toUpperName = (name) => return name.toUpperCase();

// console.log(toUpperName('Vinicius'));

//desestruturação

function handleClick (event) {
  const { clientX, clientY } = event; //cria duas constantes com os determinados nomes e adiciona tudo que e igual ao evento event

  console.log(clientX, clientY);
}

//com array 
const frutas = ['banana', 'uva'];
const [fruta1, fruta2] = frutas;
console.log(fruta2);

document.addEventListener('click', handleClick);

const useQuadrado = [
  4,
  function (lado) {
    return 4 * lado;
  },
];

const [ lados, perimetro] = useQuadrado;

console.log('quantidade de lados do quadrado: ', lados);
console.log('perimetro do quadrado: ', perimetro(10));

//Rest e Spread
function showList (empresa, ...clientes) { //os 3 pontinhos siguinifica que ele vai pegar os elementos e transformar em um array
  clientes.forEach((cliente) => {
    console.log(cliente, empresa);
  });
}

showList('Google', 'Vini', 'Raul', 'Item 2', 'Item 3')

const numeros = [10, 5, 20];
const maior = Math.max(...numeros);
console.log(maior);

const numeros2 = [7, 12, ...numeros, 30, 40, 50];
console.log(numeros2);

//adicionar elementos a um array sem o modificar

const carro = {
  cor: 'Azul',
  portas: 4
};

carroAno = { ...carro, ano: 2008 };
console.log(carroAno);